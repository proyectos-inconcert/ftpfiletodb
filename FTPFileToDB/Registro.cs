﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FTPFileToDB
{
    class Registro
    {
        public string ID_ENTIDAD;
        public string NUM_TELEFONO;
        public string VCLI_NRO_PUNTUACION_VINCULACION;

        public Registro(string iD_ENTIDAD, string nUM_TELEFONO, string vCLI_NRO_PUNTUACION_VINCULACION)
        {
            ID_ENTIDAD = iD_ENTIDAD;
            NUM_TELEFONO = nUM_TELEFONO;
            VCLI_NRO_PUNTUACION_VINCULACION = vCLI_NRO_PUNTUACION_VINCULACION;
        }

        public Registro() { }
    }
}
