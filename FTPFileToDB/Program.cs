﻿using System;
using WinSCP;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using log4net;
using log4net.Config;

namespace FTPFileToDB
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static string _hostName;
        private static string _user;
        private static string _pass;
        private static string _certificatefingerprint;
        private static string _ftpfolder;
        private static string _ftpHistoricfolder;
        private static string _tempfolder;

        private static SqlConnection _connection;
        private static string _storedProcessName;

        static void Main(string[] args)
        {

            XmlConfigurator.Configure();

            log.Info("*****************************************************");
            log.Info("Start FTPFileToDB");

            List<Registro> registros = CheckFTPFolder();

            ////List<string> archivos = new List<string>();
            //string archivo = @"C:\Users\ignacio.alfaro\OneDrive - TELECONTACT, SL\Desarrollos\FTPFileToDB\FTPFileToDB\Clientes_priorizados.csv";
            //archivos.Add(archivo);
            //List<Registro> registros = CheckArchivoCSV(archivo);

            if (registros.Count > 0)
            {
                InsertDB(registros);//Archivo a DB
            }

            log.Info("End FTPFileToDB");
            log.Info("*****************************************************");
        }

        private static void InsertDB(List<Registro> registros)
        {
            log.Info($"Start InsertDB");

            if (null != registros && registros.Count > 0)
            {
                using (_connection = new SqlConnection() { ConnectionString = ConfigurationManager.ConnectionStrings["MyDBConn"].ConnectionString })
                {
                    _connection.Open();
                    log.Info($"Establecemos conexión con BD {_connection.Database}");
                    LlamarSPInicial(_connection);
                    int num = registros.Count;
                    int index = 1;
                    foreach (Registro reg in registros)
                    {
                        var result = InsertRegDB(reg, _connection);
                        if (result)
                        {
                            log.Info($"Insertado: {index} de {num} [{reg.ID_ENTIDAD} {reg.NUM_TELEFONO} {reg.VCLI_NRO_PUNTUACION_VINCULACION}]");
                        }
                        index++;
                    }
                }
            }

        }
        internal static bool InsertRegDB(Registro reg, SqlConnection connection)
        {
            _storedProcessName = ReadSetting("StoredProcessName");
            SqlCommand command = null;
            bool insertado = false;
            log.Debug($"Se ejecuta el sp: {_storedProcessName}");

            using (command = new SqlCommand(_storedProcessName, connection) { CommandType = CommandType.StoredProcedure })
            {
                command.Parameters.Add("@ID_ENTIDAD", SqlDbType.VarChar, 250).Value = reg.ID_ENTIDAD;
                command.Parameters.Add("@NUM_TELEFONO", SqlDbType.VarChar, 250).Value = reg.NUM_TELEFONO;
                command.Parameters.Add("@VCLI_NRO_PUNTUACION_VINCULACION", SqlDbType.VarChar, 6).Value = reg.VCLI_NRO_PUNTUACION_VINCULACION;
                log.Debug($"Parámetros: {reg.ID_ENTIDAD}, {reg.NUM_TELEFONO}, {reg.VCLI_NRO_PUNTUACION_VINCULACION}");
                try
                {
                    int respuestaSp = command.ExecuteNonQuery();
                    insertado = true;
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
            }
            return insertado;
        }
        internal static void LlamarSPInicial(SqlConnection connection)
        {
            _storedProcessName = ReadSetting("StoredProcessNameInicial");
            SqlCommand command = null;
            log.Info($"Se ejecuta el sp: {_storedProcessName}");
            using (command = new SqlCommand(_storedProcessName, connection) { CommandType = CommandType.StoredProcedure })
            {
                
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
            }
        }
        private static List<Registro> CheckArchivoCSV(string stream)
        {
            log.Info($"Start CheckArchivoCSV: {stream}");
            List<Registro> registros = new List<Registro>();
            string msg = string.Empty;
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    string[] values = null;
                    Registro reg = null;
                    if (line.Contains(";"))
                    {
                        values = line.Split(';');
                    }
                    else if (line.Contains(","))
                    {
                        values = line.Split(',');
                    }
                    else
                    {
                        log.Error("El separador de las lineas no es ni ; ni ,");
                        throw new FileLoadException($"El separador de las lineas no es ni ; ni ,");
                    }

                    if (null != values)
                    {
                        int columnas = values.Length;
                        if (columnas == 3)
                        {
                            if (!values[0].Equals("ID_ENTIDAD"))
                            {
                                reg = new Registro(values[0], values[1], values[2]);
                                registros.Add(reg);
                                log.Debug($"Fila: {values[0]} {values[1]} {values[2]}");
                            }
                        }
                        else
                        {
                            log.Error($"La linea tiene {columnas} columnas. Se esperan solo 3.");
                            throw new FileLoadException($"La linea tiene {columnas} columnas. Se esperan solo 3.");
                        }
                    }
                }
            }

            log.Info($"End CheckArchivoCSV. Numero registros {registros.Count}");
            return registros;
        }
        private static List<Registro> CheckFTPFolder()
        {
            log.Info("Start CheckFTPFolder");
            List<Registro> respuesta = new List<Registro>();
            _hostName = ReadSetting("HostName");
            _user = ReadSetting("User");
            _pass = ReadSetting("Pass");
            _certificatefingerprint = ReadSetting("CertificateFingerPrint");
            _ftpfolder = ReadSetting("ftpfolder");
            _ftpHistoricfolder = ReadSetting("ftpHistoricfolder");
            _tempfolder = ReadSetting("tempfolder");
            Session session = null;
            RemoteDirectoryInfo directoryInfo = null;

            if (!Directory.Exists(_tempfolder))
            {
                Directory.CreateDirectory(_tempfolder);
                log.Info($"No existe el directorio {_tempfolder} y se crea.");
            }
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = _hostName,
                    UserName = _user,
                    Password = _pass
                    //FtpSecure = FtpSecure.Explicit,
                    //TlsHostCertificateFingerprint = _certificatefingerprint
                };
                log.Info($"Inicio de sesión FTP: {_hostName}@{_user}");
                using (session = new Session())
                {
                    session.Open(sessionOptions);
                    directoryInfo = session.ListDirectory(_ftpfolder);
                    log.Info($"Comprobación del directorio FTP: {_ftpfolder}");
                    if (null != directoryInfo && directoryInfo.Files.Count > 0)
                    {
                        TransferOptions transferOptions = new TransferOptions
                        {
                            OverwriteMode = OverwriteMode.Overwrite,
                            TransferMode = TransferMode.Automatic,
                            FilePermissions = new FilePermissions { Octal = "777" },
                            PreserveTimestamp = true
                        };
                        TransferOperationResult transferResult = session.GetFilesToDirectory(_ftpfolder, _tempfolder, null, false, transferOptions);
                        log.Info($"Se transfieren los archivos del directorio del FTP al directorio local temporal {_tempfolder} con resultado: {transferResult.IsSuccess}");
                        if (transferResult.IsSuccess)
                        {
                            string[] archivos = Directory.GetFiles(_tempfolder);
                            if (archivos.Length > 0)
                            {

                                if (!session.FileExists(_ftpHistoricfolder))
                                {
                                    log.Debug($"No existe y se crea el directorio: {_ftpHistoricfolder}");
                                    session.CreateDirectory(_ftpHistoricfolder);
                                }

                                foreach (var item in archivos)
                                {
                                    if (item.Contains(".csv") || item.Contains(".txt"))
                                    {
                                        log.Info($"Archivo encontrado: {item}");
                                        List<Registro> regs = new List<Registro>();
                                        regs = CheckArchivoCSV(item);
                                        respuesta.AddRange(regs);

                                        string archivo = Path.GetFileName(item);
                                        string marcaFecha = DateTime.Now.ToString("dd_MM_yyyy_HH_mm");
                                        string ftpInitialPath = RemotePath.Combine(_ftpfolder, archivo);
                                        string ftpFinallPath = RemotePath.Combine(_ftpHistoricfolder, $"{marcaFecha}_{archivo}");
                                        session.MoveFile(ftpInitialPath, ftpFinallPath);
                                        log.Info($"Movemos el archivo al historial: {ftpInitialPath} a {ftpFinallPath}");

                                        File.Delete(item);
                                        log.Debug($"Borrado del archivo temporal: {item}");
                                    }
                                }
                            }
                            else
                            {
                                log.Error("No se ha descargado ningun archivo a la carpeta temporal");
                            }
                        }
                        else
                        {
                            var errores = transferResult.Failures;
                            if (errores.Count > 0)
                            {
                                foreach (var err in errores)
                                {
                                    log.Error(err);
                                }
                            }
                        }
                    }
                }
            }
            catch (FileLoadException e1)
            {
                log.Error($"Error con los archivos encontrados en el FTP: {e1.Message}");
            }
            catch (Exception e)
            {
                log.Error(e);
            }

            int num = 0;
            if (null != respuesta)
            {
                num = respuesta.Count;
            }
            log.Info($"End CheckFTPFolder -> Encontrados {num} archivos");
            return respuesta;
        }
        internal static string ReadSetting(string key)
        {
            //si log 0 no hay log, si otra cosa sí hay log
            string result = string.Empty;
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "Not Found";

            }
            //   catch (ConfigurationErrorsException e)
            catch (Exception e)
            {
                log.Error(e);
            }
            return result;
        }
    }
}
