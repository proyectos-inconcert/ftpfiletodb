USE [Pruebas]
GO

/****** Object:  Table [dbo].[ClientesPriorizados]    Script Date: 20/10/2021 23:21:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClientesPriorizados](
	[ID_ENTIDAD] [varchar](250) NULL,
	[NUM_TELEFONO] [varchar](250) NULL,
	[VCLI_NRO_PUNTUACION_VINCULACION] [varchar](6) NULL
) ON [PRIMARY]
GO


USE [Pruebas]
GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_ClientesPriorizados]    Script Date: 20/10/2021 23:21:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alfaro, Ignacio
-- Create date: 20/10/2021

-- =============================================
ALTER PROCEDURE [dbo].[sp_Insert_ClientesPriorizados]
	-- Add the parameters for the stored procedure here
	@ID_ENTIDAD varchar(250), @NUM_TELEFONO varchar(250), @VCLI_NRO_PUNTUACION_VINCULACION varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
INSERT INTO [dbo].[ClientesPriorizados]
           ([ID_ENTIDAD]
           ,[NUM_TELEFONO]
           ,[VCLI_NRO_PUNTUACION_VINCULACION])
     VALUES
           (@ID_ENTIDAD, @NUM_TELEFONO, @VCLI_NRO_PUNTUACION_VINCULACION)
END
